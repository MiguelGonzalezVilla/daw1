/**
 * Created by Khondax on 07/12/2016.
 */
this.randomtip = function(){

    var pause = 6000; // define the pause for each tip (in milliseconds)
    var length = $(".tips li").length / 2;
    var temp = -1;

    this.getRan = function(){
        // get the random number
        var ran = Math.floor(Math.random()*length) + 1;
        return ran;
    };
    this.show = function(){
        var ran = getRan();
        // to avoid repeating
        while (ran == temp){
            ran = getRan();
        };
        temp = ran;
        $(".tips li").hide();
        $(".tips li:nth-child(" + ran + ")").fadeIn("fast");
    };

    show(); setInterval(show,pause);

};

$(document).ready(function(){
    randomtip();
});